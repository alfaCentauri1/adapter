package adapterPatron;
/**
 * AdaptadorBD.java implementa la interfaz IConexionSQL, en esta clase se definen y adaptan los metodos para extender la
 * funcionalidad del programa original.
 * @author Ricardo Presilla.
 * @version 1.0.
 **/
public class AdaptadorBD implements IConexionSQL{
    /**
     * Necesario para el patron adaptador.
     **/
    private IConexionNoSQL noSQL;

    /**
     * Constructor, inyecta la dependencia necesaria.
     * @param noSQL Tipo IConexionNoSQL.
     */
    public AdaptadorBD(IConexionNoSQL noSQL) {
        this.noSQL = noSQL;
    }

    /**
     * Establece la conexión a noSQL manteniendo el mismo nombre del método anterior a la aplicación del adaptador.
     */
    @Override
    public void conexion() {
        noSQL.conexion();
    }

    /**
     * Ejecuta la consulta noSQL como si fuera un query de SQL. Adapta el metodo existente para funcionar sin afectar al
     * cliente.
     **/
    @Override
    public String runQuery() {
        return noSQL.executeSentence();
    }
}
