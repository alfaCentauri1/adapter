package adapterPatron;
/**
 * ConexionMongoDB.java implementa la interfaz IConexionNoSQL; que permite utilizar MongoDB, en esta clase se definen y
 * adaptan los metodos para extender la funcionalidad del programa original.
 * @author Ricardo Presilla.
 * @version 1.0.
 **/
public class ConexionMongoDB implements IConexionNoSQL{
    @Override
    public void conexion() {
        System.out.println("Establecida la conexión con MongoDB.");
    }

    @Override
    public String executeSentence() {
        return "Consulta en noSQL";
    }
}
