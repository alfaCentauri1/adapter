package adapterPatron;
/**
 * ConexionMySQL.java implementa la interfaz IConexionSQL, en esta clase se definen y adaptan los metodos para la
 * funcionalidad del programa original.
 * @author Ricardo Presilla.
 * @version 1.0.
 **/
public class ConexionMySQL implements IConexionSQL{

    @Override
    public void conexion() {
        System.out.println("Establecida la conexión con MySQL.");
    }

    @Override
    public String runQuery() {
        return "Consulta SQL";
    }
}
