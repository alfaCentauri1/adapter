package adapterPatron;
/**
 * IConexionNoSQL.java indica que las conexiones SQL deben tener un par de métodos comunes.
 * @author Ricardo Presilla.
 * @version 1.0.
 */
public interface IConexionNoSQL {
    /**
     * Establecer la conexión al servidor.
     **/
    void conexion();
    /**
     * Ejecuta un query.
     * @return Regresa una cadena de caracteres.
     **/
    String executeSentence();
}
