package adapterPatron;
/**
 * Implementa el método main para la ejecución del programa.
 * @author Ricardo Presilla.
 * @version 1.0.
 */
public class Main {
	/**
	 * @param args the command line arguments
	 */
    public static void main(String[] args) {
//	    IConexionSQL conexionSql = new ConexionMySQL(); //Original
		IConexionSQL conexionSql = new AdaptadorBD(new ConexionMongoDB()); //Con el patron Adapter

	    conexionSql.conexion();
	    String respuesta = conexionSql.runQuery();
	    System.out.println(respuesta);
    }
}
